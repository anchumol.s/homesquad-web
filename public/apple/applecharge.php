<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$version = $_POST["version"];
$data = $_POST["data"];
$signature = $_POST["signature"];
$ephemeralPublicKey = $_POST["ephemeralPublicKey"];
$publicKeyHash = $_POST["publicKeyHash"];
$transactionId = $_POST["transactionId"];

$token_data = array(
    'version' => $version,
    'data' => $data,
    'signature' => $signature,
    'header' => array(
        'ephemeralPublicKey' => $ephemeralPublicKey,
        'publicKeyHash' => $publicKeyHash,
        'transactionId' => $transactionId,
    ),
);
//  GET TOKEN

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, "https://sandbox.checkout.com/api2/tokens");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Authorization: pk_test_b1794aa7-2f96-480b-9411-f3c655b390f6',
    'Content-Type:application/json;charset=UTF-8',
));
curl_setopt($ch, CURLOPT_POSTFIELDS,
    json_encode(array(
        'type' => 'applepay',
        'token_data' => $token_data,
    )));

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output = curl_exec($ch);

echo ('<pre>' . print_r(curl_getinfo($ch, CURLINFO_HTTP_CODE), true) . '</pre>');

curl_close($ch);

exit();
