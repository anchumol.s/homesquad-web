<?php
define('PRODUCTION_CERTIFICATE_KEY', 'certificate_sandbox.key');
// define('PRODUCTION_CERTIFICATE_PATH', 'certificate_sandbox.pem');
define('PRODUCTION_CERTIFICATE_PATH', 'certificate_liveNew.pem');
define('PRODUCTION_CERTIFICATE_KEY_PASS', ''); 
// define('PRODUCTION_MERCHANTIDENTIFIER', 'merchant.com.emaid.elitmaidsUser');
define('PRODUCTION_MERCHANTIDENTIFIER', 'merchant.com.emaid.elitmaidsUser.livennew');

define('PRODUCTION_DOMAINNAME', $_SERVER["HTTP_HOST"]);


define('PRODUCTION_CURRENCYCODE', 'AED');
define('PRODUCTION_COUNTRYCODE', 'AE');
define('PRODUCTION_DISPLAYNAME', 'Elitemaids');

define('DEBUG', 'true');
?>
