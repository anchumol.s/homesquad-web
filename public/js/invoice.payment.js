$().ready(function () {
    invoice_form_validator = $('#payment_link_form').validate({
        focusInvalid: false,
        ignore: [],
        rules: {
            "customerId": {
                required: true,
            },
            "inv_id": {
                required: false,
            },
            "amount": {
                required: true,
            },
            "description": {
                required: true
            },
        },
        messages: {
            "customerId": {
                required: "Customer ID not found",
            },
            "inv_id": {
                required: "Invoice ID not found",
            },
            "amount": {
                required: "Enter amount",
            },
            "description": {
                required: "Enter description / message",
            },
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "test") {
                error.insertAfter($('#payment-method-holder').append());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            ('#payment_link_form').submit();
        }
    });
});