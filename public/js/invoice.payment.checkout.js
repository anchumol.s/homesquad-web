let invoice_checkout_form = $('#invoice-checkout-form');
$().ready(function () {
    $('input[name="payment_method"]').change(function () {
        let payment_method = this.value;
        let booking_btn = $('#booking-form button[type="submit"]');
        $("div[class*='payment-type-']").hide();
        //$('[class^=payment-type-]').hide(); // hide all for toggle specific on next step
        $('.payment-type-' + payment_method + '').show();
        $('[id^=submit-btn-pay-mode-]').hide();
        booking_btn.attr('disabled', false);
        if (payment_method == 2) {
            _checkout_token_data = undefined;
            $('#submit-btn-pay-mode-2').show();
            booking_btn.html('Pay Now');
        }
        else if (payment_method == 3) {
            _checkout_token_data = undefined;
            booking_btn.html('');
            $('#submit-btn-pay-mode-3').show();
        }
        else if (payment_method == 4) {
            _checkout_token_data = undefined;
            booking_btn.html('');
            $('#submit-btn-pay-mode-4').show();
        }
        else if (payment_method == 5) {
            _checkout_token_data = undefined;
            $('#submit-btn-pay-mode-' + payment_method, booking_form).show();
            //TamaraTokenization();
        }
    });
    /************************************************************************** */
    // google pay
    const baseRequest = {
        apiVersion: 2,
        apiVersionMinor: 0
    };
    const tokenizationSpecification = {
        type: 'PAYMENT_GATEWAY',
        parameters: {
            'gateway': 'checkoutltd',
            'gatewayMerchantId': _checkout_primary_key
        }
    };
    const allowedCardNetworks = ["AMEX", "DISCOVER", "INTERAC", "JCB", "MASTERCARD", "VISA"];

    const allowedCardAuthMethods = ["PAN_ONLY", "CRYPTOGRAM_3DS"];

    const baseCardPaymentMethod = {
        type: 'CARD',
        parameters: {
            allowedAuthMethods: allowedCardAuthMethods,
            allowedCardNetworks: allowedCardNetworks
        }
    };
    const cardPaymentMethod = Object.assign(
        { tokenizationSpecification: tokenizationSpecification },
        baseCardPaymentMethod
    );

    const paymentsClient = new google.payments.api.PaymentsClient({ environment: _google_pay_environment });

    const isReadyToPayRequest = Object.assign({}, baseRequest);
    isReadyToPayRequest.allowedPaymentMethods = [baseCardPaymentMethod];

    paymentsClient.isReadyToPay(isReadyToPayRequest)
        .then(function (response) {
            if (response.result) {
                // add a Google Pay payment button
            }
        })
        .catch(function (err) {
            // show error in developer console for debugging
            alert('error google pay')
            console.error(err);
        });

    const button =
        paymentsClient.createButton({
            buttonColor: 'black',
            buttonType: 'plain',
            buttonSizeMode: 'fill',
            onClick: () => {
                $('#booking-form').submit();
            },
            allowedPaymentMethods: []
        });
    document.getElementById('submit-btn-pay-mode-4').appendChild(button);
    /************************************************************************** */
    invoice_checkout_form_validator = $('#invoice-checkout-form').validate({
        focusInvalid: false,
        ignore: [],
        rules: {
            "payment_method": {
                required: true
            },
            "card_number": {
                required: 'input[name="payment_method"][value="2"]:checked'
            },
            "exp_date": {
                required: 'input[name="payment_method"][value="2"]:checked'
            },
            "cvv": {
                required: 'input[name="payment_method"][value="2"]:checked'
            },
        },
        messages: {
            "payment_method": {
                required: "Select payment mode",
            },
            "card_number": {
                required: "Enter card number",
            },
            "exp_date": {
                required: "Enter card expiry date",
            },
            "cvv": {
                required: "Enter CVV code",
            },
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "payment_method") {
                error.insertAfter($('#payment-method-holder').append());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            makePayment();
            return false;
        }
    });
});
invoice_payment_req = null;
function makePayment() {
    invoice_checkout_form = $('#invoice-checkout-form');
    let payment_method = $('input[name="payment_method"]:checked', invoice_checkout_form).val();
    if (payment_method == 2) {
        if (_checkout_token_data) {
            // continue
        }
        else {
            checkoutTokenization();
            return false;
        }
    }
    invoice_payment_req = $.ajax({
        type: 'POST',
        url: _base_url + "api/customer/invoice-payment",
        dataType: 'json',
        data: {
            payment_method: $('input[name="payment_method"]:checked', invoice_checkout_form).val(),
            customer_id: $('input[name="customer_id"]', invoice_checkout_form).val(),
            order_id: $('input[name="order_id"]', invoice_checkout_form).val(),
            amount: $('input[name="amount"]', invoice_checkout_form).val(),
            checkout_token_data: _checkout_token_data
        },
        beforeSend: function () {
            if (invoice_payment_req != null) {
                invoice_payment_req.abort();
            }
        },
        success: function (response) {
            if (response.result.status == "success") {
                if (payment_method  == 2) {
                    let checkout_data = response.result.checkout_data;
                    if (checkout_data.status.toLowerCase() == "pending") {
                        // redirect to bank otp page
                        window.location.href = checkout_data._links.redirect.href;
                    }
                    else if (checkout_data.approved == true) {
                        // payment success
                        Swal.fire({
                            title: "Payment Received !",
                            html: "Payment received</b>",
                            icon: "success",
                            allowOutsideClick: false,
                            confirmButtonText: 'Show Details',
                            timer: 3000,
                        }).then((result) => {
                            if (result.isConfirmed || result.dismiss === swal.DismissReason.timer) {
                                window.location.href = _base_url;
                            }
                        });
                    }
                }
            }
            else{
                _checkout_token_data = undefined;
                toast('Error', response.result.message, 'error');
            }
        },
        error: function (response) {
        }
    });
}
function checkoutTokenization() {
    pay_btn = $('button[type="submit"]', invoice_checkout_form);
    pay_btn.html(loading_button_html);
    /********************************* Tokenization ************************************/
    $.ajax({
        type: 'POST',
        url: _checkout_token_url,
        dataType: 'json',
        data: JSON.stringify({
            "type": "card",
            "requestSource": "JS",
            "number": $('#card-details input[name="card_number"]').val(),
            "expiry_month": moment($('#card-details input[name="exp_date"]').val(), "MM/YY").format("MM"),
            "expiry_year": moment($('#card-details input[name="exp_date"]').val(), "MM/YY").format("YYYY"),
            "cvv": $('#card-details input[name="cvv"]').val()
        }),
        // important
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + _checkout_primary_key,
        },
        crossDomain: true,
        contentType: 'application/json;charset=UTF-8',
        success: function (response) {
            _checkout_token_data = response;
            makePayment();
        },
        error: function (response) {
            toast('Failed', 'Invalid card details', 'error');
            _checkout_token_data = undefined;
            pay_btn.html('Pay Now');
        },
    });
}