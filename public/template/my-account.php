	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Emaid Booking</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="theme-color" content="#0c3995">
<link rel="icon" type="image/png" href="images/favicon.png"/>

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="css/animation.css"/>
<link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">

</head>

<body>

<div class="wrapper-main">
	
    
    <?php require_once('include/header.php') ?>
	
    
    <section>
		<div class="container">
			<div class="row inner-wrapper m-0">
				 
                 <div class="col-sm-12 my-account-wrapper">
				 
				      <div class="col-sm-12"><h3>My account</h3></div>
					  
					  
					  
					  <div class="row m-0 pt-5">
					       <div class="col-sm-12 p-0">
						        <div class="col-md-3 user-details-main">
								     <div class="user-photo">
									      <img src="images/user.jpg" alt=""/>
										  <label class="" title="Edit Photo"><i class="fa fa-pencil"></i></label>
									 </div>
									 
									 <div class="user-content">
							              <h4>Mathew Thomas</h4>
										  <p>+971 234 5678</p>
									 </div>
								</div>
						   </div>
						   <div class="col-sm-12 p-0">
  
						        <div class="row user-det-thumb-main m-0 d-flex align-items-stretch">
								     
								     <div class="col-md-3 col-sm-4 user-det-thumb p-0">
									      <a href="personal-details.php">
										  		<div class="user-det-icon"><img src="images/user-icon1.png" alt="" /></div>
									      		Personal Details
										  </a>
								    </div>
									
									
									 <div class="col-md-3 col-sm-4 user-det-thumb p-0">
									      <a href="manage-address.php">
									      		<div class="user-det-icon"><img src="images/user-icon2.png" alt="" /></div>
									      		Manage Address
										  </a>
									 </div>
									 
									 <div class="col-md-3 col-sm-4 user-det-thumb p-0">
									      <a class="show-reset-password-popup">
									      		<div class="user-det-icon"><img src="images/user-icon3.png" alt="" /></div>
									      		Change Password
										  </a>
									 </div>
									 <div class="col-md-3 col-sm-4 user-det-thumb p-0">
									      <a href="upcoming-bookings.php">
									      		<div class="user-det-icon"><img src="images/user-icon4.png" alt="" /></div>
									      		Upcoming Bookings
										  </a>
									 </div>
									 <div class="col-md-3 col-sm-4 user-det-thumb p-0">
									      <a href="past-bookings.php">
									      		<div class="user-det-icon"><img src="images/user-icon5.png" alt="" /></div>
									      		Past Bookings
										  </a>
									 </div>
									 <div class="col-md-3 col-sm-4 user-det-thumb p-0">
									      <a href="index.php">
									      		<div class="user-det-icon"><img src="images/user-icon6.png" alt="" /></div>
									      		Logout
										  </a>
									 </div>
								</div>
						   </div>
					  </div>
					  
				 </div>
				 
			</div>
		</div>  
    </section>

</div>

<?php require_once('include/footer.php') ?>
          
</body>
</html>
