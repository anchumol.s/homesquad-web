	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Emaid Booking</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="theme-color" content="#0c3995">
<link rel="icon" type="image/png" href="images/favicon.png"/>

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="css/animation.css"/>
<link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">

</head>

<body>

<div class="wrapper-main">
	
    
    <?php require_once('include/header.php') ?>
	
    
    <section>
		<div class="container">
			<div class="row inner-wrapper m-0">
			
			
				 <div class="col-sm-12 top-steps-section">
				      <div class="d-flex page-title-section">
					  	   <div class="booking-page-title flex-grow-1"><h3>Date & Time</h3></div>
					  	   <div class="step-back-icon"><a href="booking-service-details.php" class="back-arrow" title="Click to Back">Step 3</a></div>
					  	   <div class="booking-steps"> of 4</div>
					  </div>
			    </div>
					  
					  
					  
				 <div class="col-lg-8 col-md-7 booking-form-left">

					  
					  <div class="col-sm-12 booking-alert-main">
					       
						   <div class="d-flex booking-alert">
							  <div class="booking-alert-icon"><i class="fa fa-refresh"></i></div>
							  <div class="booking-alert-cont flex-grow-1"><p><strong>Frequency</strong><br /><span>One Time Service</span></p></div>
							  <div class="booking-alert-btn frequency-popup-btn">Change</div>
							</div>
					       
					  </div>
					  
					  
					  
					  
					  
					  
					  
					  <div class="col-sm-12 calender-wrapper">
					       <div class="col-sm-12 p-0">
					            <h4>When would you like your service? </h4>
					       </div>
					       <div class="col-sm-12 calendar-main p-0">
						   
						         <div id="calendar" class="owl-carousel owl-theme p-0">
										<div class="item">
											<input id="date1" value="cash" name="dare" class="" type="radio">
											<label for="date1">
											<div class="calendar-tmb-main">
												<div class="calendar-tmb-day">Sun</div>
												<div class="calendar-tmb-date">01</div>
												<div class="calendar-tmb-month">Dec</div>
											</div>
											</label>
										</div>
										
										<div class="item">
											<input id="date2" value="cash" name="dare" class="" type="radio">
											<label for="date2">
											<div class="calendar-tmb-main">
												<div class="calendar-tmb-day">Mon</div>
												<div class="calendar-tmb-date">02</div>
												<div class="calendar-tmb-month">Dec</div>
											</div>
											</label>
										</div>
										
										<div class="item">
											<input id="date3" value="cash" name="dare" class="" checked="checked" type="radio">
											<label for="date3">
											<div class="calendar-tmb-main">
												<div class="calendar-tmb-day">Tue</div>
												<div class="calendar-tmb-date">03</div>
												<div class="calendar-tmb-month">Dec</div>
											</div>
											</label>
										</div>
										
										<div class="item">
											<input id="date4" value="cash" name="dare" class="" type="radio">
											<label for="date4">
											<div class="calendar-tmb-main">
												<div class="calendar-tmb-day">Wed</div>
												<div class="calendar-tmb-date">04</div>
												<div class="calendar-tmb-month">Dec</div>
											</div>
											</label>
										</div>
										
										<div class="item">
											<input id="date5" value="cash" name="dare" class="" type="radio">
											<label for="date5">
											<div class="calendar-tmb-main">
												<div class="calendar-tmb-day">Thu</div>
												<div class="calendar-tmb-date">05</div>
												<div class="calendar-tmb-month">Dec</div>
											</div>
											</label>
										</div>
										
										<div class="item">
											<input id="date6" value="cash" name="dare" class="" type="radio">
											<label for="date6">
											<div class="calendar-tmb-main">
												<div class="calendar-tmb-day">Fri</div>
												<div class="calendar-tmb-date">06</div>
												<div class="calendar-tmb-month">Dec</div>
											</div>
											</label>
										</div>
										
										<div class="item">
											<input id="date7" value="cash" name="dare" class="" type="radio">
											<label for="date7">
											<div class="calendar-tmb-main">
												<div class="calendar-tmb-day">Sat</div>
												<div class="calendar-tmb-date">07</div>
												<div class="calendar-tmb-month">Dec</div>
											</div>
											</label>
										</div>
										
										<div class="item">
											<input id="date8" value="cash" name="dare" class="" type="radio">
											<label for="date8">
											<div class="calendar-tmb-main">
												<div class="calendar-tmb-day">Sun</div>
												<div class="calendar-tmb-date">08</div>
												<div class="calendar-tmb-month">Dec</div>
											</div>
											</label>
										</div>
										
										<div class="item">
											<input id="date9" value="cash" name="dare" class="" type="radio">
											<label for="date9">
											<div class="calendar-tmb-main">
												<div class="calendar-tmb-day">Mon</div>
												<div class="calendar-tmb-date">09</div>
												<div class="calendar-tmb-month">Dec</div>
											</div>
											</label>
										</div>
										
										<div class="item">
											<input id="date10" value="cash" name="dare" class="" type="radio">
											<label for="date10">
											<div class="calendar-tmb-main">
												<div class="calendar-tmb-day">Tue</div>
												<div class="calendar-tmb-date">10</div>
												<div class="calendar-tmb-month">Dec</div>
											</div>
											</label>
										</div>
										
										
										<div class="item">
											<input id="date11" value="cash" name="dare" class="" type="radio">
											<label for="date11">
											<div class="calendar-tmb-main">
												<div class="calendar-tmb-day">Wed</div>
												<div class="calendar-tmb-date">11</div>
												<div class="calendar-tmb-month">Dec</div>
											</div>
											</label>
										</div>
										
										<div class="item">
											<input id="date12" value="cash" name="dare" class="" type="radio">
											<label for="date12">
											<div class="calendar-tmb-main">
												<div class="calendar-tmb-day">Thu</div>
												<div class="calendar-tmb-date">12</div>
												<div class="calendar-tmb-month">Dec</div>
											</div>
											</label>
										</div>
										
										<div class="item">
											<input id="date13" value="cash" name="dare" class="" type="radio">
											<label for="date13">
											<div class="calendar-tmb-main">
												<div class="calendar-tmb-day">Fri</div>
												<div class="calendar-tmb-date">13</div>
												<div class="calendar-tmb-month">Dec</div>
											</div>
											</label>
										</div>
										
										<div class="item">
											<input id="date14" value="cash" name="dare" class="" type="radio">
											<label for="date14">
											<div class="calendar-tmb-main">
												<div class="calendar-tmb-day">Sat</div>
												<div class="calendar-tmb-date">14</div>
												<div class="calendar-tmb-month">Dec</div>
											</div>
											</label>
										</div>
										
										<div class="item">
											<input id="date15" value="cash" name="dare" class="" type="radio">
											<label for="date15">
											<div class="calendar-tmb-main">
												<div class="calendar-tmb-day">Sun</div>
												<div class="calendar-tmb-date">15</div>
												<div class="calendar-tmb-month">Dec</div>
											</div>
									
											</label>
										</div>
										
										<div class="item">
											<input id="date16" value="cash" name="dare" class="" type="radio">
											<label for="date16">
											<div class="calendar-tmb-main">
												<div class="calendar-tmb-day">Mon</div>
												<div class="calendar-tmb-date">16</div>
												<div class="calendar-tmb-month">Dec</div>
											</div>
											</label>
										</div>
										
										<div class="item">
											<input id="date17" value="cash" name="dare" class="" type="radio">
											<label for="date17">
											<div class="calendar-tmb-main">
												<div class="calendar-tmb-day">Tue</div>
												<div class="calendar-tmb-date">17</div>
												<div class="calendar-tmb-month">Dec</div>
											</div>
											</label>
										</div>
										
										<div class="item">
											<input id="date18" value="cash" name="dare" class="" type="radio">
											<label for="date18">
											<div class="calendar-tmb-main">
												<div class="calendar-tmb-day">Wed</div>
												<div class="calendar-tmb-date">18</div>
												<div class="calendar-tmb-month">Dec</div>
											</div>
											</label>
										</div>
										
										<div class="item">
											<input id="date19" value="cash" name="dare" class="" type="radio">
											<label for="date19">
											<div class="calendar-tmb-main">
												<div class="calendar-tmb-day">Thu</div>
												<div class="calendar-tmb-date">19</div>
												<div class="calendar-tmb-month">Dec</div>
											</div>
											</label>
										</div>
										
										<div class="item">
											<input id="date20" value="cash" name="dare" class="" type="radio">
											<label for="date20">
											<div class="calendar-tmb-main">
												<div class="calendar-tmb-day">Fri</div>
												<div class="calendar-tmb-date">20</div>
												<div class="calendar-tmb-month">Dec</div>
											</div>
											</label>
										</div>
										
									</div>

								
					       </div>
					  </div>
					  
					  
					  
					  
					  <div class="col-sm-12 what-time-wrapper">
					       <div class="col-sm-12 p-0 pb-2">
					            <h4>What time would you like us to start?  <label class="what-time-btn">See All</label></h4>
					       </div>
					       <div class="col-sm-12 booking-form-list p-0">
					            <ul>
								    <li>
									    <input id="booking-time1" value="cash" name="booking-time" class="" type="radio">
                                        <label for="booking-time1">08:00-08:30</label>
									</li>
									
									<li>
									    <input id="booking-time2" value="cash" name="booking-time" class="" type="radio">
                                        <label for="booking-time2"><p>AED 5 Extra</p>08:30-09:00</label>
									</li>
									
									<li>
									    <input id="booking-time3" value="cash" name="booking-time" class="" type="radio">
                                        <label for="booking-time3">09:00-09:30</label>
									</li>
									
									<li>
									    <input id="booking-time4" value="cash" name="booking-time" class="" type="radio">
                                        <label for="booking-time4">09:30-10:00</label>
									</li>
									
									<li>
									    <input id="booking-time5" value="cash" name="booking-time" class="" type="radio">
                                        <label for="booking-time5">11:00-11:30</label>
									</li>
								</ul>
					       </div>
					  </div>
					  
					  
					  
					  <div class="col-sm-12 which-housekeeper-wrapper">
					       <div class="col-sm-12 p-0 pb-2">
					            <h4>Which Housekeeper  do you prefer?</h4>
					       </div>
					       <div class="col-sm-12 p-0">
								
								<div id="housekeepers" class="owl-carousel owl-theme p-0">

									 <div class="item">
										  <input id="which-maid1" value="cash" name="which-maid" checked="checked" class="" type="radio">
                                          <label for="which-maid1">
											  <div class="which-housekeeper-thumb">
											   <div class="which-housekeeper-thumb-image"><img src="images/auto-maid.jpg" alt="" /></div>
											   <div class="which-housekeeper-thumb-name v-center">Auto-assign</div>
											   <div class="which-housekeeper-thumb-auto-assign">We’ll the best Housekeeper</div>
										  </div>
										  </label>
									 </div>
									 
									 
									 <div class="item">
										  <input id="which-maid2" value="cash" name="which-maid" class="" type="radio">
                                          <label for="which-maid2">
											  <div class="which-housekeeper-thumb">
											       <div class="which-housekeeper-thumb-rating">4.5</div>
												   <div class="which-housekeeper-thumb-image"><img src="images/maid.jpg" alt="" /></div>
												   <div class="which-housekeeper-thumb-name v-center">Elizabeth Delyn</div>
												   <div class="which-housekeeper-thumb-auto-assign">Recommended in your area</div>
											  </div>
										  </label>
									 </div>
									 
									 
									 <div class="item">
										  <input id="which-maid3" value="cash" name="which-maid" class="" type="radio">
                                          <label for="which-maid3">
											  <div class="which-housekeeper-thumb">
											       <div class="which-housekeeper-thumb-rating">4.5</div>
												   <div class="which-housekeeper-thumb-image"><img src="images/maid.jpg" alt="" /></div>
												   <div class="which-housekeeper-thumb-name v-center">Elizabeth Delyn</div>
												   <div class="which-housekeeper-thumb-auto-assign">Recommended in your area</div>
											  </div>
										  </label>
									 </div>
									 
									 
									 <div class="item">
										  <input id="which-maid4" value="cash" name="which-maid" class="" type="radio">
                                          <label for="which-maid4">
											  <div class="which-housekeeper-thumb">
											       <div class="which-housekeeper-thumb-rating">4.5</div>
												   <div class="which-housekeeper-thumb-image"><img src="images/maid.jpg" alt="" /></div>
												   <div class="which-housekeeper-thumb-name v-center">Elizabeth Delyn</div>
												   <div class="which-housekeeper-thumb-auto-assign">Recommended in your area</div>
											  </div>
										  </label>
									 </div>
									 
									 
									 <div class="item">
										  <input id="which-maid5" value="cash" name="which-maid" class="" type="radio">
                                          <label for="which-maid5">
											  <div class="which-housekeeper-thumb">
											       <div class="which-housekeeper-thumb-rating">4.5</div>
												   <div class="which-housekeeper-thumb-image"><img src="images/maid.jpg" alt="" /></div>
												   <div class="which-housekeeper-thumb-name v-center">Elizabeth Delyn</div>
												   <div class="which-housekeeper-thumb-auto-assign">Recommended in your area</div>
											  </div>
										  </label>
									 </div>
									 
									 
									 <div class="item">
										  <input id="which-maid6" value="cash" name="which-maid" class="" type="radio">
                                          <label for="which-maid6">
											  <div class="which-housekeeper-thumb">
											       <div class="which-housekeeper-thumb-rating">4.5</div>
												   <div class="which-housekeeper-thumb-image"><img src="images/maid.jpg" alt="" /></div>
												   <div class="which-housekeeper-thumb-name v-center">Elizabeth Delyn</div>
												   <div class="which-housekeeper-thumb-auto-assign">Recommended in your area</div>
											  </div>
										  </label>
									 </div>
									 
									 
									 <div class="item">
										  <input id="which-maid7" value="cash" name="which-maid" class="" type="radio">
                                          <label for="which-maid7">
											  <div class="which-housekeeper-thumb">
											       <div class="which-housekeeper-thumb-rating">4.5</div>
												   <div class="which-housekeeper-thumb-image"><img src="images/maid.jpg" alt="" /></div>
												   <div class="which-housekeeper-thumb-name v-center">Elizabeth Delyn</div>
												   <div class="which-housekeeper-thumb-auto-assign">Recommended in your area</div>
											  </div>
										  </label>
									 </div>
					            </div>
								
					       </div>
					  </div>

					  
					  
					  
					  <div class="col-sm-12 booking-alert-main booking-cancellation-alert">
						   <div class="d-flex booking-alert">
							  <div class="booking-alert-icon"><i class="fa fa-exclamation-triangle"></i></div>
							  <div class="booking-alert-cont flex-grow-1"><p><strong>Booking Cancellation</strong><br /><span>Free cancellation until 12 hours before the start of your booking. </span></p></div>
							  <div class="booking-alert-btn">Details</div>
							</div>
					  </div>
					  
					  
					  
				 
				 </div>
				 
				 
				 <div class="col-lg-4 col-md-5 booking-summary-section">
				      <div class="col-lg-11 col-md-12 booking-summary-main clearfix scroll">
					  <div class="row m-0">
					       <div class="col-sm-12 book-details-main pb-2">
                                <h3>Booking Summary</h3>
						   </div>
                                      
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-6 book-det-left ps-0 pe-0"><p>Service</p></div>
									 <div class="col-6 book-det-right ps-0 pe-0"><p>Home Cleaning</p></div>
								</div>
						   </div> 
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-6 book-det-left ps-0 pe-0"><p>Service Details</p></div>
									 <div class="col-6 book-det-right ps-0 pe-0"><p>3x Cupboard Cleaning</p></div>
								</div>
						   </div> 
                                      
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-6 book-det-left ps-0 pe-0"><p>Frequency</p></div>
									 <div class="col-6 book-det-right ps-0 pe-0"><p>One Time Service</p></div>
								</div>
						   </div>
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-6 book-det-left ps-0 pe-0"><p>Duration</p></div>
									 <div class="col-6 book-det-right ps-0 pe-0"><p>2 hours</p></div>
								</div>
						   </div>
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-5 book-det-left ps-0 pe-0"><p>Date & Time</p></div>
									 <div class="col-7 book-det-right ps-0 pe-0"><p>27 Sept 2023,<br />05:00 pm  to  07:00 pm</p></div>
								</div>
						   </div>
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-7 book-det-left ps-0 pe-0"><p>Number of Professionals</p></div>
									 <div class="col-5 book-det-right ps-0 pe-0"><p>5</p></div>
								</div>
						   </div>
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-6 book-det-left ps-0 pe-0"><p>Material</p></div>
									 <div class="col-6 book-det-right ps-0 pe-0"><p>No</p></div>
								</div>
						   </div>
						   
						   
						   <div class="col-sm-12 book-details-main">
								<div class="row m-0">
									 <div class="col-5 book-det-left ps-0 pe-0"><p>Crew</p></div>
									 <div class="col-7 book-det-right ps-0 pe-0"><p>Elizabeth Delya</p></div>
								</div>
						   </div>  
						   
						   </div>
						   
						   
						   <div class="row book-details-main-set m-0 pt-5">
						   
							    <div class="col-sm-12 book-details-main pb-0">
                                     <h3>Payment Summary</h3>
						        </div>
								



								
								
								<div class="col-sm-12 book-details-main mt-3">
								  <div class="row booking-amount m-0">
									  <div class="col-6 book-det-left ps-0 pe-0"><p>Service Fee</p></div>
									  <div class="col-6 book-det-right ps-0 pe-0"><p><span>AED</span> 93.33</p></div>
								  </div>
								</div>
							  
							  
								<div class="col-sm-12 book-details-main">
								  <div class="row booking-amount m-0">
									  <div class="col-6 book-det-left ps-0 pe-0"><p>Total (inc VAT 5%)</p></div>
									  <div class="col-6 book-det-right ps-0 pe-0"><p><span>AED</span> 4.67</p></div>
								  </div>
								</div>
							  
							  
							  
								<div class="col-sm-12 book-details-main">
								  <div class="row total-price m-0">
									  <div class="col-5 book-det-left ps-0 pe-0"><p>Total</p></div>
									  <div class="col-7 book-det-right ps-0 pe-0"><p><span>AED</span> 98.00</p></div>
								  </div>
								</div>
								
						  </div>     
                      </div>
				 
				 </div>
				 
				 
				 <div class="col-sm-12 booking-main-btn-section">
				 <div class="row m-0">
						   <div class="col-lg-3 col-md-4 col-sm-6 p-0 pt-3">
						        <a href="javascript:void(0);" class="show-login-popup">
							    <input value="Next" class="text-field-btn" type="submit">
								</a>
						   </div>
					  </div>
				</div>
				 
			</div>
		</div>  
    </section>

</div>

<?php require_once('include/footer.php') ?>
          
</body>
</html>
