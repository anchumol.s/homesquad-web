<footer class="footer-wrapper">
	   
	    <div class="footer-home">
		     <div class="container">
			      <div class="quick-links">
					  <h4>Cleaning Services</h4>
					  <ul>
						  <li><a href="#">House Cleaning  Services</a></li>
						  <li><a href="#">Carpet Cleaning Services</a></li>
						  <li><a href="#">Deep Cleaning Services</a></li>
						  <li><a href="#">Laundry Service in Duabi</a></li>
						  <li><a href="#">Mattress Cleaning Services</a></li>
						  <li><a href="#">Office Cleaning Service</a></li>
						  <li><a href="#">Sofa Cleaning Services</a></li>
						  <li><a href="#">Water Tank Cleaning Services</a></li>
						  <li><a href="#">Window Cleaning Services</a></li> 
					  </ul>
				  </div>
				  
				  
				  <div class="quick-links">
					  <h4>Salon & Spa at Home</h4>
					  <ul>
						  <li><a href="#">House Cleaning  Services</a></li>
						  <li><a href="#">Carpet Cleaning Services</a></li>
						  <li><a href="#">Deep Cleaning Services</a></li>
						  <li><a href="#">Laundry Service in Duabi</a></li>
						  <li><a href="#">Mattress Cleaning Services</a></li>
						  <li><a href="#">Office Cleaning Service</a></li>
						  <li><a href="#">Sofa Cleaning Services</a></li>
						  <li><a href="#">Water Tank Cleaning Services</a></li>
						  <li><a href="#">Window Cleaning Services</a></li> 
						  <li><a href="#">House Cleaning  Services</a></li>
						  <li><a href="#">Carpet Cleaning Services</a></li>
						  <li><a href="#">Deep Cleaning Services</a></li>
						  <li><a href="#">Laundry Service in Duabi</a></li>
						  <li><a href="#">Mattress Cleaning Services</a></li>
						  <li><a href="#">Office Cleaning Service</a></li>
						  <li><a href="#">Sofa Cleaning Services</a></li>
						  <li><a href="#">Water Tank Cleaning Services</a></li>
						  <li><a href="#">Window Cleaning Services</a></li> 
					  </ul>
				  </div>
				  
				  
				  <div class="quick-links">
					  <h4>Healthcare at Home</h4>
					  <ul>
						  <li><a href="#">House Cleaning  Services</a></li>
						  <li><a href="#">Carpet Cleaning Services</a></li>
						  <li><a href="#">Deep Cleaning Services</a></li>
						  <li><a href="#">Laundry Service in Duabi</a></li>
						  <li><a href="#">Mattress Cleaning Services</a></li>

					  </ul>
				  </div>
				  
				  
				  <div class="other-quick-links">
					  <ul>
						  <li><a href="#">Home</a></li>
						  <li><a href="#">Login</a></li>
						  <li><a href="#">Offers</a></li>
						  <li><a href="#">Terms and Conditions</a></li>
						  <li><a href="#">Privacy Policy</a></li>
					  </ul>
					  
					  <p>Emaid © 2023 All Rights Reserved. &nbsp;&nbsp; | </p>

								<div class="designed">
								     <a href="https://emaid.info/" target="_blank">
									    <div class="azinova-logo"></div>
									 </a>
									 <p class="no-padding">Powered by :</p>
									 <div class="clear"></div>
								</div>

					 
				  </div>
			 </div>
		</div>
	   
	   <div class="container">
	   <div class="row footer-bottom m-0">
          <div class="col-sm-6 p-0 footer-copy">
            <p>2023 © <span>Emaid Booking.</span> All rights reserved.</p>
          </div>
          <div class="col-sm-6 p-0 footer-design">
            <div class="designed">
			<a href="https://emaid.info/" target="_blank">
              <div class="azinova-logo"></div>
            </a>
              <p class="no-padding">Powered by :</p>
              <div class="clear"></div>
            </div>
          </div>
        </div>
		</div>
	</footer>
	
	
	
<div class="popup-main frequency-popup">
     <div class="row min-vh-100 m-0">
          <div class="mx-auto my-auto shadow popup-main-cont">
               
               <div class="popup-close"><img src="images/el-close-white.png" alt=""></div>
			   
                    <div class="col-sm-12 popup-head-text"><h4>Choose Your Frequency</h4></div>
			   
			   
			   
			   <div class="row m-0">
			   
			        <div class="col-sm-12 frequency-main one-time">
					     <input id="frequency1" value="cash" name="how-often" class="" type="radio">
                         <label for="frequency1"> <span></span>One Time
						    <div style="width: 100%; height:7px;">&nbsp;</div>
						 	<p>The same professional will join you every week.</p>
						 	<p>Pause/cancel your subscription at your convenience.</p>
						 </label>
						 
						 <!--<div class="frequency-offer">5%<span>OFF</span></div>-->
					</div>
					
					<div class="col-sm-12 frequency-main weekly">
					     <div class="frequency-label">Most popular</div>
					     <input id="frequency2" value="card" name="how-often" class="" type="radio">
                         <label for="frequency2"> <span></span>Weekly <strong>10% OFF</strong>
						    <div style="width: 100%; height:7px;">&nbsp;</div>
						    <p>This service for the same ay every week</p>
						 </label>
						 
						 <!--<div class="frequency-offer">10%<span>OFF</span></div>-->
					</div>
					
					
					<div class="col-sm-12 frequency-main every-2week border-0">
					     <input id="frequency3" value="card" name="how-often" class="" type="radio">
                         <label for="frequency3"> <span></span>Every 2 Week <strong>15% OFF</strong>
						    <div style="width: 100%; height:7px;">&nbsp;</div>
						    <p>This service for the same ay every week</p>
						 </label>
					</div>
					
                    <div class="col-sm-6 frequency-main pt-3 border-0">
					     <a href="date-and-time.php">
					     <input value="Select Frequency" class="text-field-btn" type="submit">
						 </a>
					</div>                             
												 
										   
			   </div>
			   
			   
          </div>
     </div>
</div><!-- Frequency Popup-->





<div class="popup-main login-popup">
     <div class="row min-vh-100 m-0">
          <div class=" mx-auto my-auto shadow popup-main-cont">
               
               <div class="popup-close"><img src="images/el-close-white.png" alt=""></div>
			   
                    <div class="col-sm-12 popup-head-text"><h4>Login /Sign up</h4></div>
					
			   
			   
			   
			   <div class="row m-0">
			   
			        <div class="col-sm-12 login-content p-0">
			             <p>We are glad to have you here. please login or Signup to complete your  boking.</p>
					</div>
					
					<div class="col-sm-12 login-field">
					     <input name="" class="text-field" type="number">
						 <div class="country-code">+971</div>
					</div>
					
					
			        
                    <div class="col-sm-12 frequency-main pt-3">
					     <input value="Continue" class="text-field-btn SHOW-OTP-BTN" type="submit"><!--Remove SHOW-OTP-BTN class from button field while programming. its using for show the demo flow-->
					</div>                             				   
			   </div>
			   
			   
          </div>
     </div>
</div><!-- Login Popup-->
	
	
	


<div class="popup-main otp-popup">
     <div class="row min-vh-100 m-0">
          <div class=" mx-auto my-auto shadow popup-main-cont">
               
               <div class="popup-close"><img src="images/el-close-white.png" alt=""></div>
			   
                    <div class="col-sm-12 popup-head-text"><h4>Verify phone number</h4></div>
					
			   
			   
			   
			   <div class="row m-0">
			   
			        <div class="col-sm-12 otp-content">
			             <p><strong>Enter the code that was sent to</strong></p>
						 <h5>+971 50 123 4556</h5>
					</div>
					
					<div class="col-sm-12 otp-field p-0">
						 <input type="number" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==4) return false;"  class="text-field" />
					</div>
					
					
					<div class="col-sm-12 otp-field p-0">
					     <p><strong>Didn't receive a code?</strong></p>
						 <a href="#" class="pull-left">Resend SMS</a>         <a href="#" class="pull-right">Send code via WhatsApp</a>
					</div>
					
					
			        
                    <div class="col-sm-12 frequency-main pt-3">
					     <a href="checkout.php">
					     <input value="Continue" class="text-field-btn" type="submit">
						 </a>
					</div>                             				   
			   </div>
			   
			   
          </div>
     </div>
</div><!-- OTP Popup-->





<div class="popup-main add-address-popup">
     <div class="row min-vh-100 m-0">
          <div class=" mx-auto my-auto shadow popup-main-cont">
               
               <div class="popup-close"><img src="images/el-close-white.png" alt=""></div>
			   
                    <div class="col-sm-12 popup-head-text"><h4>Add Address</h4></div>
			   
					   <div class="row m-0">
					   
					        <div class="col-md-6 address-google-map">
								<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3613.4846948105287!2d55.154268710472095!3d25.08544900023128!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f134e609581e1%3A0x5312bbe8506b0c9c!2sEmirates%20Golf%20Club-%20Majilis%20Course.!5e0!3m2!1sen!2sin!4v1590989224759!5m2!1sen!2sin" allowfullscreen="" aria-hidden="false" tabindex="0" width="100%" frameborder="0"></iframe>
							</div>
							
							
							<div class="col-md-6 address-details">
							     <div class="row m-0">
								 
								      <div class="col-sm-12 address-options p-0 pb-4">
									       <ul>
												 <li class="home">
													 <input id="address1" value="address" name="address-opt" class="" checked="" type="radio">
													 <label for="address1">Home</label>
												 </li>
												 
												 
												 <li class="office">
													 <input id="address2" value="address" name="address-opt" class="" type="radio">
													 <label for="address2">Office</label>
												 </li>
												 
												 
												 <li class="other">
													 <input id="address3" value="address" name="address-opt" class="" type="radio">
													 <label for="address3">Other</label>
												 </li>
										   </ul>
									  </div>
									  
								      
									  <div class="col-sm-12 text-field-main">
											 <input name="" placeholder="Street" class="text-field" type="text">
									  </div>
									  
									  
									  <div class="col-sm-12 p-0">
										   <div class="row text-field-main">
										        <div class="col-sm-8">
											         <input name="" placeholder="Building" class="text-field" type="text">
										        </div>
												
												<div class="col-sm-4">
											         <input name="" placeholder="Flat No:" class="text-field" type="text">
										        </div>
										   </div>
									  </div>
									  
									  
									  <div class="col-sm-12 text-field-main">
										   <select name="area">
										       <option value="0">Select your area</option>
											   <option value="1">Area 1</option>
											   <option value="2">Area 2</option>
											   <option value="3">Area 3</option>
											   <option value="4">Area 4</option>
											   <option value="5">Area 5</option>
										   </select>
									  </div>
									  
									  
									  <div class="col-sm-12 text-field-main">
										   <input value="Add Address" class="text-field-btn" type="submit">
									  </div>
									  
								 </div>
							</div>
					   
							

                          				   
					   </div>

          </div>
     </div>
</div><!-- Address Popup-->





<div class="popup-main promocode-popup">
     <div class="row min-vh-100 m-0">
          <div class=" mx-auto my-auto shadow popup-main-cont">
               
               <div class="popup-close"><img src="images/el-close-white.png" alt=""></div>
			   
                    <div class="col-sm-12 popup-head-text"><h4>Promo Codes</h4></div>
			   
					   <div class="row m-0">
					   
					        <div class="col-sm-12 promo-code-field">
							     <input name="" placeholder="Enter Promo Code" class="text-field" type="text">
							</div>

							
							<div class="col-sm-12 p-0">
								 <h5>Available Codes</h5>
							</div>
							
					   <div class="col-sm-12 promocode-cont-set">
						   
							
							<div class="d-flex promocode-cont-main">
							  <div class="promocode-cont flex-grow-1">
							       <p><strong>SP5368</strong><br>
								   <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span></p>
							  </div>
							  <div class="promocode-cont-btn v-center">
							       <input value="Apply" class="text-field-btn" type="submit">
							  </div>
							</div>
							
							
							<div class="d-flex promocode-cont-main">
							  <div class="promocode-cont flex-grow-1">
							       <p><strong>SP5368</strong><br>
								   <span>Lorem Ipsum is simply dummy text of the printing Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span></p>
							  </div>
							  <div class="promocode-cont-btn v-center">
							       <input value="Apply" class="text-field-btn" type="submit">
							  </div>
							</div>
							
							
							<div class="d-flex promocode-cont-main">
							  <div class="promocode-cont flex-grow-1">
							       <p><strong>SP5368</strong><br>
								   <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span></p>
							  </div>
							  <div class="promocode-cont-btn v-center">
							       <input value="Apply" class="text-field-btn" type="submit">
							  </div>
							</div>
							
					  </div>
							
							
							
							<div class="col-sm-6 frequency-main pt-3">
								 <input value="Confirm" class="text-field-btn" type="submit">
							</div>                             				   
					   </div>

          </div>
     </div>
</div><!-- Promo Code Popup-->





<div class="popup-main reset-password-popup">
     <div class="row min-vh-100 m-0">
          <div class=" mx-auto my-auto shadow popup-main-cont">
               <div class="popup-close"><img src="images/el-close-white.png" alt=""></div>
                    <div class="col-sm-12 popup-head-text"><h4>Reset Password</h4></div>
					
			   
			   <div class="row m-0 pt-1">
			   

					<div class="col-sm-12 reset-password-field-main p-0">
					     <p>Old Password</p>
						 <div class="reset-password-field">
						      <input type="password"  class="text-field" />
						      <div class="password-show-btn fa fa-fw fa-eye-slash"></div>
						 </div>
					</div>
					
					<div class="col-sm-12 reset-password-field-main p-0">
					     <p>New Password</p>
						 <div class="reset-password-field">
						      <input type="password"  class="text-field" />
						      <div class="password-show-btn fa fa-fw fa-eye-slash"></div>
						 </div>
					</div>
					
					<div class="col-sm-12 reset-password-field-main p-0">
					     <p>Confirm Password</p>
						 <div class="reset-password-field">
						      <input type="password"  class="text-field" />
						      <div class="password-show-btn fa fa-fw fa-eye-slash"></div>
						 </div>
					</div>
			        
                    <div class="col-sm-12 frequency-main p-0 pt-3">
					     <input value="Reset" class="text-field-btn" type="submit">
					</div>                             				   
			   </div>
			   
			   
          </div>
     </div>
</div><!-- Reset Password Popup-->





<div class="popup-main cancel-booking-popup">
     <div class="row min-vh-100 m-0">
          <div class=" mx-auto my-auto shadow popup-main-cont">
               <div class="popup-close"><img src="images/el-close-white.png" alt=""></div>
                    <div class="col-sm-12 popup-head-text"><h4>Cancel Booking</h4></div>
					
			   
			   <div class="row m-0 pt-1">
			   
                    <div class="col-sm-12 p-0 mb-3">
					     <p class="text-red">Are you sure want to cancel the boking?</p>
						 <h5><strong>16 Oct, 2023 5:04 pm</strong> ( One Day Booking)</h5>
					</div>
					
					<div class="col-sm-12 maids-section clearfix p-0 pt-1">
							   <div class="pb-maids"><img src="images/maid.jpg" alt=""></div>
							   <div class="pb-maids"><img src="images/maid.jpg" alt=""></div>
							   <div class="pb-maids"><img src="images/maid.jpg" alt=""></div>
						  </div>
			        
					<div class="col-sm-12 p-0 pt-3">
                    <div class="row pt-3">
					     <div class="col-sm-6 mb-2"><a class="close-popup"><input value="Keep Booking" class="text-field-btn" type="submit"></a></div>
						 <div class="col-sm-6"><a href="cancelled-booking.php"><input value="Cancel Booking" class="text-field-btn" type="submit"></a></div>
					</div> 
					</div>                            				   
			   </div>
			   
			   
          </div>
     </div>
</div><!-- Cancel Booking Popup-->





<div class="popup-main packages-details-popup">
	<div class="row min-vh-100 m-0">
		<div class=" mx-auto my-auto shadow popup-main-cont">
			<div class="popup-close"><img src="images/el-close-white.png" alt=""></div>
			<div class="row m-0">
				 <div class="col-12 p-0"><img src="images/default.png" alt=""></div>
				 
				 <div class="col-12 packages-details-popup-cont">
				      <h3>1 Bedroom</h3>
					  <p>Enjoy a healthy and refreshing environment after we complete deep cleaning your entire homeEnjoy a healthy and refreshing environment after we complete deep cleaning your entire home Enjoy a healthy and refreshing environment after we complete deep cleaning your entire home.</p>
					  
					  <div class="col-12 booking-packages-price p-0 pt-3">
						   <label>AED <span>456</span> 356</label>
					  </div>
									
					  <ul>
					      <li>Enjoy a healthy and refreshing environment after we complete</li>
						  <li>Enjoy a healthy and refreshing environment after we complete refreshing environment after we complete</li>
						  <li>Enjoy a healthy and refreshing</li>
					  </ul>
				 </div>
				 
			     <div class="col-12 pak-det-popup-bot-btn">Back to Booking</div>
			
			</div>
		</div>
	</div>
</div><!-- Packages Details Popup-->



<script type="text/javascript" src="js/jquery-3.6.0.min.js"></script>
<script type="text/javascript" src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/jquery.fixie.js"></script> 
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/datepicker.js"></script>
<script type="text/javascript" src="js/jquery.jscroll.js"></script>







<script type="text/javascript">
$(document).ready(function() {
	

        var strategy = undefined;
    var match = document.location.search.match(/strategy=(.\S+)/);
    if (match) strategy = match[1];

    $(function() {
      
	  
      $('.sticker').fixie({
        topMargin: 70,
        pinSlop: 0,
        strategy: strategy,
        //pinnedBodyClass: 'showGlobalChange'
      });
    });
	
	
	
	var owl = $("#package-category");
	owl.owlCarousel({
	  items: 7,
      autoWidth:true,
	  loop:false,
	  nav: true,
	  dots: false,
	  margin: 10,
	  autoplay: false,
	  autoplayTimeout: 2000,
	  smartSpeed: 800,
	  autoplayHoverPause: false
	});
	
	
	var owl = $("#banner");
	owl.owlCarousel({
	  items: 1,
	  loop: true,
	  margin: 0,
	  autoplay: true,
	  autoplayTimeout: 4000,
	  autoplayHoverPause: true
	});
	
	
	var owl = $("#category");
	owl.owlCarousel({
	  nav: true,
	  loop: true,
	  dots:false,
	  margin: 10,
	  autoplay: true,
	  autoplayTimeout: 2000,
	  smartSpeed: 800,
	  autoplayHoverPause: true,
	  responsive: {
                  0: {
                    items: 2
                  },
                  600: {
                    items: 3
                  },
                  1000: {
                    items: 5
                  },
				  1100: {
                    items: 5
                  },
				  1200: {
                    items: 7,
					margin: 15
                  }
	 }
	});
	
	
	var owl = $("#service1,#service2,#service3,#service4,#service5");
	owl.owlCarousel({
	  nav: false,
	  loop: false,
	  dots: true,
	  margin: 15,
	  autoplay: true,
	  autoplayTimeout: 2000,
	  smartSpeed: 800,
	  autoplayHoverPause: true,
	  responsive: {
                  0: {
                    items: 1
                  },
                  600: {
                    items: 3
                  },
                  1000: {
                    items: 3
                  },
				  1100: {
                    items: 4
                  },
				  1200: {
                    items: 5,
					margin: 30
                  }
	 }
	});
	
	
	
	var owl = $("#housekeepers");
	owl.owlCarousel({
	  nav: false,
	  loop: false,
	  dots:true,
	  margin: 15,
	  autoplay: false,
	  autoplayTimeout: 2000,
	  smartSpeed: 800,
	  autoplayHoverPause: true,
	  responsive: {
                  0: {
                    items: 2
                  },
                  600: {
                    items: 4
                  },
                  1000: {
                    items: 5
                  },
				  1100: {
                    items: 5
                  },
				  1200: {
                    items: 6,
					//margin: 50
                  }
	 }
	});
	
	
	
	var owl = $("#add-ons");
	owl.owlCarousel({
	  nav: false,
	  loop: false,
	  dots:true,
	  margin: 15,
	  autoplay: true,
	  autoplayTimeout: 2000,
	  smartSpeed: 800,
	  autoplayHoverPause: true,
	  responsive: {
                  0: {
                    items: 1
                  },
                  600: {
                    items: 2
                  },
                  1000: {
                    items: 3
                  },
				  1100: {
                    items: 3
                  },
				  1200: {
                    items: 3,
					//margin: 50
                  }
	 }
	});
	

	var owl = $("#calendar");
	owl.owlCarousel({
	  nav: true,
	  loop: false,
	  dots: false,
	  margin: 15,
	  autoplay: false,
	  autoplayTimeout: 2000,
	  smartSpeed: 800,
	  autoplayHoverPause: false,
	  responsive: {
                  0: {
                    items: 5
                  },
                  600: {
                    items: 5
                  },
                  1000: {
                    items: 10
                  },
				  1100: {
                    items: 10
                  },
				  1200: {
                    items: 10,
					//margin: 50
                  }
	 }
	});

	
	$('.play').on('click', function() {
	  owl.trigger('play.owl.autoplay', [2000])
	})
	$('.stop').on('click', function() {
	  owl.trigger('stop.owl.autoplay')
	})
	
	
	$('.user-btn').click(function() {
		$('.mobile-dropdown').toggle( 500);
	});
	
	
	$('.popup-close, .close-popup').click(function() {
		$('.popup-main').hide( 500);
	});
	
	
	$('.frequency-popup-btn').click(function() {
		$('.frequency-popup').show( 500);
	});
	
	$('.show-login-popup').click(function() {
		$('.login-popup').show( 500);
	});
	

	$('.SHOW-OTP-BTN').click(function() {
		$('.otp-popup').show( 500);
		$('.login-popup').hide( 500);
	});
	
	
	$('.show-add-address-popup').click(function() {
		$('.add-address-popup').show( 500);
	});
	
	
	$('.show-promocode-popup').click(function() {
		$('.promocode-popup').show( 500);
	});
	
	
	$('.show-reset-password-popup').click(function() {
		$('.reset-password-popup').show( 500);
	});
	
	$('.voucher-close').click(function() {
		$('.voucher-code-credit').hide( 500);
	});
	
	
	$('.show-pak-det-popup').click(function() {
		$('.packages-details-popup').show( 500);
	});
	
	$('.pak-det-popup-bot-btn').click(function() {
		$('.packages-details-popup').hide( 500);
	});

	
	
	//$(".manage-address-section").click(function(){
//		$('.manage-address-section').removeClass('active');
//		$(this).toggleClass('active');
//    });
	
	

	
	
	$(".delete-action").click(function(){
	    $(this).closest(".manage-address-section").remove();
	});
	
	$(".scroll").jScroll();
	
})


</script>


