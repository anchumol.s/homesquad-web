	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Emaid Booking</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="theme-color" content="#0c3995">
<link rel="icon" type="image/png" href="images/favicon.png"/>

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="css/animation.css"/>
<link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">

</head>

<body>

<div class="wrapper-main">
	
    
    <?php require_once('include/header.php') ?>
	
    
    <section>
		<div class="container">
			<div class="row inner-wrapper m-0">
			
			     <div class="col-sm-12 top-steps-section">
					  <div class="d-flex page-title-section">
						  <div class="booking-page-title flex-grow-1"><h3>Upcoming Bookings</h3></div>
						  <div class="step-back-icon"><a href="my-account.php" class="back-arrow" title="Click to Back">Back</a></div>
						  <div class="booking-steps pt-0"><div class="booking-back ps-2"><a href="past-bookings.php" class="mt-0">Past Bookings</a></div></div>
					  </div>
				 </div>
				 
				 
                 <div class="col-sm-12 my-account-wrapper">
					  
					  <div class="row m-0 pt-3">
					  
					       <div class="col-md-8 col-sm-12 n-personal-details m-auto pt-0">
					       
						   <div class="co-sm-12 past-bookings-main">
						        <div class="d-flex">
							         <div class="past-bookings-content flex-grow-1">
									      <div class="col-sm-12 p-0">
									      	   <h4>House Cleaning</h4>
										       <p>16 Oct, 2023 5:04 pm ( One Day Booking)</p>
										  </div>
										  <div class="col-sm-12 maids-section clearfix p-0">
										       <div class="pb-maids"><img src="images/maid.jpg" alt="" /></div>
											   <div class="pb-maids"><img src="images/maid.jpg" alt="" /></div>
											   <div class="pb-maids"><img src="images/maid.jpg" alt="" /></div>
										  </div>
									 </div>
									 <div class="past-bookings-status v-center">
									      <h4>Ref: 5689</h4>
										  <p class="cancelled"><a class="show-cancel-booking-popup">Cancel</a></p>
									 </div>
							    </div>
						   </div>
						   
						   
						   <div class="co-sm-12 past-bookings-main">
						        <div class="d-flex">
							         <div class="past-bookings-content flex-grow-1">
									      <div class="col-sm-12 p-0">
									      	   <h4>House Cleaning</h4>
										       <p>16 Oct, 2023 5:04 pm ( One Day Booking)</p>
										  </div>
										  <div class="col-sm-12 maids-section clearfix p-0">
										       <div class="pb-maids"><img src="images/maid.jpg" alt="" /></div>
											   <div class="pb-maids"><img src="images/maid.jpg" alt="" /></div>
											   <div class="pb-maids"><img src="images/maid.jpg" alt="" /></div>
											   <div class="pb-maids"><img src="images/maid.jpg" alt="" /></div>
											   <div class="pb-maids"><img src="images/maid.jpg" alt="" /></div>
										  </div>
									 </div>
									 <div class="past-bookings-status v-center">
									      <h4>Ref: 5689</h4>
										  <p class="cancelled"><a class="show-cancel-booking-popup">Cancel</a></p>
									 </div>
							    </div>
						   </div>
						   
						   
						   <div class="co-sm-12 past-bookings-main">
						        <div class="d-flex">
							         <div class="past-bookings-content flex-grow-1">
									      <div class="col-sm-12 p-0">
									      	   <h4>House Cleaning</h4>
										       <p>16 Oct, 2023 5:04 pm ( One Day Booking)</p>
										  </div>
										  <div class="col-sm-12 maids-section clearfix p-0">
										       <div class="pb-maids"><img src="images/maid.jpg" alt="" /></div>
											   <div class="pb-maids"><img src="images/maid.jpg" alt="" /></div>
										  </div>
									 </div>
									 <div class="past-bookings-status v-center">
									      <h4>Ref: 5689</h4>
										  <p class="cancelled"><a class="show-cancel-booking-popup">Cancel</a></p>
									 </div>
							    </div>
						   </div>
						   
						   
						   <div class="co-sm-12 past-bookings-main">
						        <div class="d-flex">
							         <div class="past-bookings-content flex-grow-1">
									      <div class="col-sm-12 p-0">
									      	   <h4>House Cleaning</h4>
										       <p>16 Oct, 2023 5:04 pm ( One Day Booking)</p>
										  </div>
										  <div class="col-sm-12 maids-section clearfix p-0">
										       <div class="pb-maids"><img src="images/maid.jpg" alt="" /></div>
											   <div class="pb-maids"><img src="images/maid.jpg" alt="" /></div>
											   <div class="pb-maids"><img src="images/maid.jpg" alt="" /></div>
										  </div>
									 </div>
									 <div class="past-bookings-status v-center">
									      <h4>Ref: 5689</h4>
										  <p class="cancelled"><a class="show-cancel-booking-popup">Cancel</a></p>
									 </div>
							    </div>
						   </div>
						   
						   
						   <div class="co-sm-12 past-bookings-main">
						        <div class="d-flex">
							         <div class="past-bookings-content flex-grow-1">
									      <div class="col-sm-12 p-0">
									      	   <h4>House Cleaning</h4>
										       <p>16 Oct, 2023 5:04 pm ( One Day Booking)</p>
										  </div>
										  <div class="col-sm-12 maids-section clearfix p-0">
										       <div class="pb-maids"><img src="images/maid.jpg" alt="" /></div>
											   <div class="pb-maids"><img src="images/maid.jpg" alt="" /></div>
										  </div>
									 </div>
									 <div class="past-bookings-status v-center">
									      <h4>Ref: 5689</h4>
										  <p class="cancelled"><a class="show-cancel-booking-popup">Cancel</a></p>
									 </div>
							    </div>
						   </div>
						   
						   </div>
						   
					  </div>
				 </div>
			</div>
		</div>  
    </section>

</div>

<?php require_once('include/footer.php') ?>
          
</body>
</html>
