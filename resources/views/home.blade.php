@extends('layouts.main', ['body_css_class' => 'home-page'])
@section('title', 'Home')
@section('content')
    <section class="banner-section">
        <!--<div class="container">-->
        <div class="row banner-wrapper m-0">
            <div id="banner" class="owl-carousel owl-theme p-0">
                @foreach ($banners as $key => $banner)
                    {{-- @if (@$banner['offer_id']) --}}
                    <div class="item position-relative" onclick="return false;" data-action="banner-select"
                        data-service_type_model_id="{{ @$banner['service_type_model_id'] }}"
                        data-service_type_model="{{ @$banner['service_type_model'] }}"
                        data-web_url_slug="{{ @$banner['web_url_slug'] }}" data-offer_id="{{ @$banner['offer_id'] }}"
                        data-coupon_code="{{ @$banner['coupon_code'] }}" data-package_id="{{ @$banner['package_id'] }}">
                        <a href="#">
                            <img src="{{ @$banner['banner_image_url_new'] ?: $banner['banner_image_url'] }}" class="object-fit_cover pc-view" alt="" />
                            <img src="{{ @$banner['banner_image_url_new'] ?: $banner['banner_image_url'] }}" class="object-fit_cover mob-view" alt="" />
                        </a>
                    </div>
                    {{-- @endif --}}
                @endforeach
            </div>
        </div>
        <!--</div>-->
    </section>
    @foreach ($api_data['service_categories'] as $category_key => $service_category)
        <section class="all-service-wrapper">
            <div class="container">
                <h3>{{ $service_category['service_type_category_name'] }}</h3>
                <div class="row m-0">
                    <div class="owl-carousel owl-theme p-0 service-categories">
                        @foreach ($service_category['sub_categories'] as $sub_category_key => $service)
                            <a href="#" onclick="return false;" data-action="service-select"
                                data-service_type_model_id="{{ $service['service_type_model_id'] }}"
                                data-service_type_model="{{ $service['service_type_model'] }}"
                                data-web_url_slug="{{ $service['web_url_slug'] }}">
                                <div class="item position-relative">
                                    <div class="col-sm-12 service-image"><img
                                            src="{{ $service['service_type_thumbnail'] }}" class=""
                                            style="width: 200px;height: 150px;"
                                            alt="{{ $service['service_type_name'] }}" /></div>
                                    <div class="col-sm-12 service-text">
                                        <h3>{{ $service['service_type_name'] }}</h3>
                                    </div>
                                </div>
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    @endforeach
    <section class="app-download-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 app-download-left v-center">
                    <div>
                        <h2>We are Available on<br />App Store & Google Play</h2>
                        <p>Download the app from Google Play or App Store and start enjoying the benefits right away.</p>
                        <a href="https://play.google.com/store/apps/details?id=com.azinova.homesquad"><img src="{{ asset('images/android.jpg') }}?v=1.0" alt="" /></a>
                        <a href="https://apps.apple.com/in/app/homesquad/id1504032314"><img src="{{ asset('images/iOS.jpg') }}?v=1.0" alt="" /></a>
                    </div>
                </div>
                <div class="col-sm-6 app-download-right"><img src="{{ asset('images/app.png') }}?v=1.0"
                        alt="" />
                </div>
            </div>
        </div>
    </section>
@endsection
@push('styles')
@endpush
@push('scripts')
<script type="text/javascript" src="{{ asset('js/home.js?v=') . Config::get('version.js') }}"></script>
@endpush
