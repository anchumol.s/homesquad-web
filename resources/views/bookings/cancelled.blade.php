@extends('layouts.main', [])
@section('title', 'Cancelld Bookings')
@section('content')
    <section>
        <div class="container">
            <div class="row inner-wrapper m-0">
                <div class="col-sm-12 top-steps-section">
                    <div class="d-flex page-title-section">
                        <div class="booking-page-title flex-grow-1">
                            <h3>Cancelled Bookings</h3>
                        </div>
                        <div class="step-back-icon"><a href="{{ url('profile') }}" class="back-arrow"
                                title="Click to Back">Back</a>
                        </div>
                        <div class="booking-steps pt-0">
                            <div class="booking-back ps-2"><a href="{{ url('bookings/upcoming') }}" class="mt-0">Upcoming
                                    Bookings</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 my-account-wrapper">
                    <div class="row m-0">
                        @if (sizeof($booking_history['result']['completed_list']) == 0)
                            <div class="col-sm-8 m-auto p-0">
                                <div class="alert alert-info" role="alert">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                    {{ $booking_history['result']['message'] }}
                                </div>
                            </div>
                        @else
                            <div class="col-sm-8 n-personal-details m-auto p-0">
                                @foreach ($booking_history['result']['completed_list'] as $key => $booking)
                                    <div class="co-sm-12 upcoming-booking-cont-main booking-cancelled">
                                        <div class="upcoming-booking-cont">
                                            <div class="row past-bookings-content m-0">
                                                <div class="col-sm-12 p-0">
                                                    <div class="row m-0">
                                                        <div class="col-md-8 past-bookings-content-left p-0">
                                                            <h4>{{ $booking['service'] }}</h4>
                                                            <p><i class="fa fa-calendar" aria-hidden="true"></i>
                                                                {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $booking['date'])->format('d M, Y') }}

                                                                &nbsp; &nbsp;

                                                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                                {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $booking['date'])->format('h:i A') }}

                                                            </p>
                                                            <p><i class="fa fa-repeat"
                                                                    aria-hidden="true"></i>&nbsp;{{ $booking['frequency'] }}
                                                            </p>
                                                        </div>
                                                        <div class="col-md-4 past-bookings-content-right p-0">
                                                            <h4 class="text-right">{{ $booking['booking_reference'] }}</h4>
                                                            <p class="text-right text-red">{{ $booking['status'] }}</p>
                                                            <p class="text-right text-red"><i class="fa fa-calendar text-black"
                                                                    aria-hidden="true"></i>
                                                                {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $booking['cancelled_date'])->format('d/m/Y') }}
                                                            </p><p class="text-right text-red"><i class="fa fa-clock-o text-black" aria-hidden="true"></i>
                                                        {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $booking['cancelled_date'])->format('h:i A') }}
                                                            </p>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 p-0 pt-2">
                                                    <h5 class="booking-amount-text">
                                                        <b>{{ number_format($booking['_total_payable'] ?: $booking['total'], 2, '.', ',') }}</b>
                                                        <span>AED</span>
                                                    </h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('styles')
@endpush
@push('scripts')
@endpush
