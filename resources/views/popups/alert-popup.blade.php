<div class="popup-main" id="alert-popup">
    <div class="row min-vh-100 m-0">
        <div class=" mx-auto my-auto shadow popup-main-cont">
            <div class="popup-close" data-action="close"><img src="{{ asset('images/el-close-white.png') }}"
                    alt=""></div>
            <div class="col-sm-12 popup-head-text">
                <h4>Alert</h4>
            </div>
            <div class="row m-0 mt-3">
                <p class="text" style="word-wrap: break-word;"><!-- Content Here --></p>
            </div>
            <div class="row">
                <div class="col-sm-12 m-auto" id="addresses-list">
                    <div class="col-sm-12 booking-main-btn-section p-0 pt-4">
                        <div class="row m-0">
                            <div class="col-sm-12 p-0 pt-3">
                                <button class="text-field-btn" data-action="close">OK</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
