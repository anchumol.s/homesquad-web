<div class="popup-main" id="avatar-crop-popup">
    <form id="avatar-crop-popup-form" novalidate="novalidate">
        <input type="hidden" name="image" />
        <div class="row min-vh-100 m-0">
            <div class=" mx-auto my-auto shadow popup-main-cont">
                <div class="popup-close" data-action="close"><img src="{{ asset('images/el-close-white.png') }}"
                        alt=""></div>
                <div class="col-sm-12 popup-head-text">
                    <h4>Crop Image</h4>
                </div>
                <div class="row m-0">
                    <div class="col-sm-12 login-field">
                        <img id="image" src="#">
                    </div>
                    <div class="col-sm-12 frequency-main pt-3">
                        <button class="text-field-btn" type="button" name="update-avatar" id="crop">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
