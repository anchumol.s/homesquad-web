<div class="popup-main address-list-popup" id="cancel-schedule-popup">
    <form id="cancel-schedule-popup-form">
        <input name="booking_id" type="hidden" />
        <div class="row min-vh-100 m-0">
            <div class=" mx-auto my-auto shadow popup-main-cont">
                <div class="popup-close" data-action="close"><img src="{{ asset('images/el-close-white.png') }}"
                        alt=""></div>
                <div class="col-sm-12 popup-head-text">
                    <h4>Confirm Cancel Schedule</h4>
                </div>
                <div class="row m-0 mt-3">
                    <div class="col-sm-12 calender-wrapper">
                        <div class="col-sm-12 p-0">
                            <h4>When would you like to reschedule? </h4>
                        </div>
                        <div class="col-sm-12 calendar-main p-0">
                            <div id="calendar" class="owl-carousel owl-theme p-0">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 what-time-wrapper">
                    <div class="col-sm-12 p-0 pb-2">
                        <h4>What time would you like us to start?
                        </h4>
                    </div>
                    <div class="col-sm-12 booking-form-list p-0">
                        <ul id="times-holder">
                        </ul>
                    </div>
                </div>
                <div class="d-flex booking-alert">
                    <div class="booking-alert-icon"><i class="fa fa-exclamation-triangle text-danger"></i></div>
                    <div class="booking-alert-cont flex-grow-1">
                        <p><strong>Cancellation Policy</strong></p>
                        <p><span>Free cancellation within 12 hours, 50% charge applies between 12 hours and the time of booking.</span></p>
                    </div>
                </div>
                <div class="row m-0">
                    <div class="col-sm-12 m-auto" id="addresses-list">
                        <div class="col-sm-12 booking-main-btn-section p-0 pt-4">
                            <div class="row m-0">
                                <div class="col-lg-5 col-md-4 col-sm-6 p-0 pt-3">
                                    <button class="text-field-btn" type="submit">Reschedule</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div><!-- Address Popup-->
