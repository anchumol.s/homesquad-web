<div class="popup-main packages-details-popup">
    <div class="row min-vh-100 m-0">
        <div class=" mx-auto my-auto shadow popup-main-cont">
            <div class="popup-close"><img src="{{asset('images/el-close-white.png')}}" alt=""></div>
            <div class="row m-0">
                <div class="col-12 p-0 package_tumbnail"></div>

                <div class="col-12 packages-details-popup-cont">
                    <h3><span class="package_details_name"></span></h3>
                    <p><span class="package_details_description"></span></p>

                    <div class="col-12 booking-packages-price p-0 pb-5 pt-3">
                        <label class="package_amount"></label>
                    </div>

                    <div class="col-sm-12 p-0 info_html">
                    </div>
                </div>

                <div class="col-12 pak-det-popup-bot-btn">Back to Booking</div>

            </div>
        </div>
    </div>
</div><!-- Packages Details Popup-->
