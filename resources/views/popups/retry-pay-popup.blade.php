<div class="popup-main address-list-popup" id="retry-pay-popup">
    <form id="retry-pay-popup-form">
        <input name="booking_id" type="hidden" />
        <input name="payment_method" type="hidden" />
        <div class="row min-vh-100 m-0">
            <div class=" mx-auto my-auto shadow popup-main-cont">
                <div class="popup-close" data-action="close"><img src="{{ asset('images/el-close-white.png') }}"
                        alt=""></div>
                <div class="col-sm-12 popup-head-text">
                    <h4>Retry Payment</h4>
                </div>
                <div class="row m-0 mt-3">
                    <div class="col-sm-12 p-0">
                        <div class="col-sm-12 card-method-main payment-type-2 card-details" style="display: none">
                            <div class="row card-main-box m-0" style="width: 100%">
                                <div class="col-sm-12 card-method-field">
                                    <p>Card Number</p>
                                    <input name="card_number" class="text-field" type="number"
                                        value="">
                                </div>

                                <div class="col-sm-8 card-method-field">
                                    <p>Exp. Date (MM/YY)</p>
                                    <!--<input name="" class="text-field" type="number">-->
                                    <input id="datepicker" class="text-field w90 calendar" name="exp_date"
                                        type="text" autocomplete="off" data-provide="datepicker" value="">
                                </div>

                                <div class="col-sm-4 card-method-field">
                                    <p>CVV Number</p>
                                    <input name="cvv" class="text-field" type="number" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row m-0">
                    <div class="col-sm-12 m-auto p-0" id="addresses-list">
                        <div class="col-sm-12 booking-main-btn-section p-0">
                            <div class="row m-0">
                                <div class="col-lg-5 col-md-4 col-sm-6 p-0" id="retry-btn-pay-mode-1">
                                    <button class="text-field-btn" type="submit">Pay Now</button>
                                </div>
                                <div class="col-lg-5 col-md-4 col-sm-6 p-0" id="retry-btn-pay-mode-2">
                                    <button class="text-field-btn" type="submit">Card Pay</button>
                                </div>
                                <div class="col-lg-5 col-md-4 col-sm-6 p-0" id="retry-btn-pay-mode-3">
                                    <button class="text-field-btn" type="submit">Apple Pay</button>
                                </div>
                                <div class="col-lg-5 col-md-4 col-sm-6 p-0" id="retry-btn-pay-mode-4">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div><!-- Address Popup-->
