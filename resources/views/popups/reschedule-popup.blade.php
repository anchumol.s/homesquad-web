<div class="popup-main address-list-popup" id="reschedule-popup">
    <form id="reschedule-popup-form">
        <input name="booking_id" type="hidden" />
        <input name="hours" type="hidden" />
        <div class="row min-vh-100 m-0">
            <div class=" mx-auto my-auto shadow popup-main-cont">
                <div class="popup-close" data-action="close"><img src="{{ asset('images/el-close-white.png') }}"
                        alt=""></div>
                <div class="col-sm-12 popup-head-text">
                    <h4><span class="booking_id"></span></h4>
                </div>
                <div class="row m-0 mt-3">
                    <div class="col-sm-12 calender-wrapper">
                        <div class="col-sm-12 p-0">
                            <h4>When would you like to reschedule? </h4>
                        </div>
                        <div class="col-sm-12 calendar-main p-0">
                            <div id="calendar" class="owl-carousel owl-theme p-0">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 what-time-wrapper">
                    <div class="col-sm-12 p-0 pb-2">
                        <h4>What time would you like us to start?
                        </h4>
                    </div>
                    <div class="col-sm-12 booking-form-list p-0">
                        <ul id="times-holder">
                        </ul>
                    </div>
                </div>
                <div class="d-flex booking-alert">
                    <div class="booking-alert-icon"><i class="fa fa-exclamation-triangle text-danger"></i></div>
                    <div class="booking-alert-cont flex-grow-1">
                        <p><strong>Reschedule Policy</strong></p>
                        <p><span>Reschedule policy is free of charge but it is not eligible for special offers and promo code availed.</span></p>
                    </div>
                </div>
                <div class="row m-0">
                    <div class="col-sm-12 p-0 m-auto" id="addresses-list">
                        <div class="col-sm-12 booking-main-btn-section">
                            <div class="row m-0">
                                <div class="col-lg-5 col-md-4 col-sm-6 p-0">
                                    <button class="text-field-btn" type="submit">Reschedule</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div><!-- Address Popup-->
