<div class="popup-main login-popup" id="login-popup">
    <form id="login-popup-form" novalidate="novalidate">
        <div class="row min-vh-100 m-0">
            <div class=" mx-auto my-auto shadow popup-main-cont">
                <div class="popup-close" data-action="close"><img src="{{asset('images/el-close-white.png')}}" alt=""></div>
                <div class="col-sm-12 popup-head-text">
                    <h4>Login / Sign up</h4>
                </div>
                <div class="row m-0">
                    <div class="col-sm-12 login-content p-0">
                        <p>We are glad to have you here. please login or Signup to complete your booking.</p>
                    </div>
                    <div class="col-sm-12 login-field">
                        <input name="country_code" type="hidden" value="91">
                        <input name="mobilenumber" class="text-field no-arrow" type="number"
                            value="{{ strpos($_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'], '127.0.0.1') !== false ? '703408618' : '' }}" inputmode="numeric">
                        <div class="country-code">+971</div>
                    </div>
                    <div class="col-sm-12 frequency-main pt-3">
                        <button class="text-field-btn"
                            type="submit">Continue</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div><!-- Login Popup-->
