<div class="col-sm-12 top-steps-section step-1">
    <div class="d-flex page-title-section">
        <div class="booking-page-title flex-grow-1">
            <h3>Date</h3>
        </div>
        <div class="step-back-icon"><a href="{{url('')}}" class="back-arrow" title="Click to Back">Step
                1</a></div>
        <div class="booking-steps"> of 2</div>
    </div>
</div>
<div class="col-lg-8 col-md-12 booking-form-left step-1">
    <div class="col-sm-12 calender-wrapper">
        <div class="col-sm-12 p-0">
            <h4>When would you like your service? </h4>
        </div>
        <div class="col-sm-12 calendar-main p-0">
            <div id="calendar-load">
            </div>
            <div id="calendar" class="owl-carousel owl-theme p-0">
            </div>
        </div>
    </div>
    <div class="col-sm-12 cleaning-materials-wrapper">
        <div class="col-sm-12 p-0 pb-2">
            <h4>Any instructions or special requirements?</h4>
        </div>
        <div class="col-sm-12 booking-form-field p-0">
            <textarea name="instructions" rows="" class="text-field-big" spellcheck="false"
                placeholder="Write here..." maxlength="300"></textarea>
                <p class="text-muted" id="ins-char-left">300 characters left</p>
        </div>
    </div>
</div>