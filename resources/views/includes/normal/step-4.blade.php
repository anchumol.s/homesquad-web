<div class="col-sm-12 top-steps-section step-4" style="display: none">
    <div class="d-flex page-title-section">
        <div class="booking-page-title flex-grow-1">
            <h3>Checkout</h3>
        </div>
        <div class="step-back-icon"><a href="javascript:void(0);" data-action="prev-step" data-step="4" class="back-arrow"
                title="Click to Back">Step
                4</a></div>
        <div class="booking-steps"> of 4</div>
    </div>
</div>
<div class="col-lg-8 col-md-12 booking-form-left step-4" style="display: none">
    <div class="col-sm-12 payment-method-wrapper pb-2">
        <div class="col-sm-12 p-0 pb-2">
            <h4>Payment method</h4>
        </div>
        <div class="col-sm-12 booking-form-list payment-method p-0">
            <ul id="payment-method-holder">
                @foreach ($api_data['payment_types'] as $key => $payment_type)
                    @if ($payment_type['show_in_web'] == 1)
                        @if (!in_array($payment_type['id'], hidePaymentModes('normal-model')))
                            <li class="{{ $payment_type['code'] }}-opt pay-mode-li-{{ $payment_type['id'] }}">
                                <input id="payment-method-{{ $payment_type['id'] }}" value="{{ $payment_type['id'] }}"
                                    name="payment_method" class="" type="radio"
                                    {{ $payment_type['default'] == 1 ? 'checked' : '' }}>
                                <label for="payment-method-{{ $payment_type['id'] }}">
                                    <!-- <p>Payment by</p>{{ $payment_type['name'] }} -->
                                    <img
                                        src="{{ asset('images/payment-' . $payment_type['code'] . '.jpg?v=' . Config::get('version.img')) }}" />
                                </label>
                            </li>
                        @else
                            {{-- @if (in_array(session('customer_id'), debugPaymentModeForCustomers()))
                                <li class="{{ $payment_type['code'] }}-opt">
                                    <input id="payment-method-{{ $payment_type['id'] }}"
                                        value="{{ $payment_type['id'] }}" name="payment_method" class=""
                                        type="radio" {{ $payment_type['default'] == 1 ? 'checked' : '' }}>
                                    <label for="payment-method-{{ $payment_type['id'] }}">
                                        <p>Payment by</p>{{ $payment_type['name'] }}
                                    </label>
                                </li>
                            @endif --}}
                        @endif
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
    <div class="col-sm-12 card-method-main mb-5 pt-3 payment-type-2" style="display: none" id="card-details">
        <div class="row card-main-box m-0">
            <div class="col-sm-12 card-method-field">
                <p>Card Number</p>
                <input name="card_number" class="text-field" type="number" value="">
            </div>

            <div class="col-sm-8 card-method-field">
                <p>Exp. Date (MM/YY)</p>
                <!--<input name="" class="text-field" type="number">-->
                <input id="datepicker" class="text-field w90 calendar" name="exp_date" type="text" autocomplete="off"
                    data-provide="datepicker" value="" placeholder="MM / YY">
            </div>

            <div class="col-sm-4 card-method-field">
                <p>CVV Number</p>
                <input name="cvv" class="text-field" type="number" value="">
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <h4>Add a voucher code</h4>

        <div class="row">
            <div class="col-sm-4 voucher-code-credit position-relative" id="applied-coupon-widget" style="display:none">
                <div class="voucher-close" data-action="remove-coupon"><img
                        src="{{ asset('images/el-close-white.png') }}" alt="close"></div>
                <div class="col-sm-12 voucher-code-tmb position-relative">
                    <p class="pt-0">Voucher Code</p>
                    <p><strong class="coupon-code">&nbsp;</strong></p>
                </div>
            </div>
            <div class="col-sm-4" id="add-coupon-widget">
                <div class="col-sm-12 voucher-code-tmb">
                    <p class="pt-0">Voucher Code</p>
                    <div class="addon-btn-main">
                        <a class="sp-btn" data-action="coupon-apply-popup">Add</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
