@include('popups.login-popup')
@include('popups.otp-popup')
@include('popups.required-details-entry-popup')
<footer class="footer-wrapper">
    <div class="footer-home">
        <div class="container">
            <div class="quick-links">
                <h4>Cleaning Services</h4>
                <ul>
                    @foreach ($api_data['service_categories'] as $service_category)
                        @foreach ($service_category['sub_categories'] as $service)
                            <li><a href="{{ $service['web_url_slug'] }}">{{ $service['service_type_name'] }}</a>
                            </li>

                        @endforeach
                    @endforeach
                </ul>
            </div>
            <div class="quick-links">
                <h4>Subscription Packages</h4>
                <ul>
                    @foreach ($api_data['subscription_packages_and_special_offers'] as $package)
                        @if (@$package['package_id'])
                            <li><a
                                    href="{{ url('/package/' . $package['package_id']) }}">{{ $package['package_name'] }}</a>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
            <div class="other-quick-links">
                <ul>
                    <li><a href="{{ url('') }}">Home</a></li>
                    {{-- <li><a href="https://elitemaids.ae/terms-and-conditions" target="_blank">Terms and Conditions</a>
                    </li>
                    <li><a href="https://elitemaids.ae/privacy-policy" target="_blank">Privacy Policy</a></li> --}}
                </ul>
                <p>Home Squad © {{ date('Y') }} All Rights Reserved. &nbsp;&nbsp; | </p>
                <div class="designed">
                    <a href="https://www.homesquad.ae/" target="_blank">
                        <div class="azinova-logo"></div>
                    </a>
                    <p class="no-padding">Powered by </p>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row footer-bottom m-0">
            <div class="col-sm-6 p-0 footer-copy">
                <p>{{ date('Y') }} © <span>Home Squad.</span> All rights reserved.</p>
            </div>
            <div class="col-sm-6 p-0 footer-design">
                <div class="designed"> <a href="https://www.homesquad.ae/" target="_blank">
                    <a href=""><img src="https://www.homesquad.ae/assets/images/home-squad-logo.png" alt=""></a>
                        <div class="azinova-logo"></div>
                    </a>
                    <p class="no-padding">Powered by </p>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
</footer>
