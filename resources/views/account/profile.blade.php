@extends('layouts.main', [])
@section('title', 'Profile')
@section('content')
    <section>
        <div class="container">
            <div class="row inner-wrapper m-0">
                <div class="col-sm-12 my-account-wrapper">
                    <div class="col-sm-12">
                        <h3>My Account</h3>
                    </div>
                    <div class="row m-0 pt-5">
                        <div class="col-sm-12 p-0">
                            <div class="col-sm-3 user-details-main">
                                <div class="user-photo">
                                    <img src="{{ session('customer_avatar') }}" alt="" />
                                    {{-- <label class="" title="Edit Photo" data-action="select-file"><i class="fa fa-pencil"></i></label> --}}
                                    <label class="" title="Edit Photo" data-action="select-file">
                                        <i class="fa fa-pencil"></i>
                                        <input type="file" id="image-input" accept="image/*" style="display: none;" />
                                        <input type="hidden" id="profile_image_base64" name="profile_image_base64" />
                                    </label>
                                </div>

                                <div class="user-content">
                                    <h4>{{ session('customer_name') ?: '-' }}</h4>
                                    <p>{{ session('customer_mobile') ?: '-' }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 p-0">
                            <div class="row user-det-thumb-main m-0 d-flex align-items-stretch">

                                <div class="col-sm-3 user-det-thumb p-0">
                                    <a href="{{ url('profile/personal-details') }}">
                                        <div class="user-det-icon"><img src="{{ asset('images/user-icon1.png') }}"
                                                alt="" /></div>
                                        Personal Details
                                    </a>
                                </div>
                                <div class="col-sm-3 user-det-thumb p-0">
                                    <a href="{{ url('profile/manage-address') }}">
                                        <div class="user-det-icon"><img src="{{ asset('images/user-icon2.png') }}"
                                                alt="" /></div>
                                        Manage Address
                                    </a>
                                </div>
                                <!--<div class="col-sm-3 user-det-thumb p-0">
                                                                                                                    <a class="show-reset-password-popup">
                                                                                                                        <div class="user-det-icon"><img src="{{ asset('images/user-icon3.png') }}" alt="" /></div>
                                                                                                                        Change Password
                                                                                                                    </a>
                                                                                                                </div>-->
                                <div class="col-sm-3 user-det-thumb p-0">
                                    <a href="{{ url('bookings/upcoming') }}">
                                        <div class="user-det-icon"><img src="{{ asset('images/user-icon4.p') }}ng"
                                                alt="" /></div>
                                        Upcoming Bookings
                                    </a>
                                </div>
                                <div class="col-sm-3 user-det-thumb p-0">
                                    <a href="{{ url('bookings/past') }}">
                                        <div class="user-det-icon"><img src="{{ asset('images/user-icon4.p') }}ng"
                                                alt="" /></div>
                                        Past Bookings
                                    </a>
                                </div>
                                <div class="col-sm-3 user-det-thumb p-0">
                                    <a href="{{ url('bookings/cancelled') }}">
                                        <div class="user-det-icon"><img src="{{ asset('images/user-icon5.png') }}"
                                                alt="" /></div>
                                        Cancelled Bookings
                                    </a>
                                </div>
                                <div class="col-sm-3 user-det-thumb p-0">
                                    <a href="javascript:void(0);" data-action="logout">
                                        <div class="user-det-icon"><img src="{{ asset('images/user-icon6.png') }}"
                                                alt="" /></div>
                                        Logout
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('popups.avatar-crop-popup')
    </section>
@endsection
@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.12/cropper.min.css">
@endpush
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.12/cropper.min.js"></script>
    <script type="text/javascript" defer>
        var avatar_crop_form = $('#avatar-crop-popup-form');
        var cropper;
        $('[data-action="close"]', avatar_crop_form).click(function() {
            closeCropper();
        });

        function showCropper(image, cropWidth, cropHeight) {
            console.log('showCropper called');
            cropper = new Cropper(image, {
                aspectRatio: cropWidth / cropHeight,
                viewMode: 1,
            });
            $('#avatar-crop-popup').show(500);
        }
        window.addEventListener('DOMContentLoaded', function() {
            var image = document.getElementById('image');
            var input = document.querySelector('label[data-action="select-file"] input[type="file"]');
            var cropButton = document.getElementById('crop');
            input.addEventListener('change', function(e) {
                var files = e.target.files;
                var done = function(url) {
                    input.value = '';
                    image.src = url;
                    image.onload = function() {
                        showCropper(image, 500, 500);
                    };
                };
                var reader;
                var file;
                if (files && files.length > 0) {
                    file = files[0];
                    if (URL) {
                        done(URL.createObjectURL(file));
                    } else if (FileReader) {
                        reader = new FileReader();
                        reader.onload = function(e) {
                            done(reader.result);
                        };
                        reader.readAsDataURL(file);
                    }
                }
            });
            if (cropButton) {
                cropButton.addEventListener('click', function() {
                    var canvas;
                    if (cropper) {
                        canvas = cropper.getCroppedCanvas({
                            width: 500,
                            height: 500,
                        });
                        var src = canvas.toDataURL();
                        $('input[name="image"]', avatar_crop_form).val(src);
                        let update_btn = $('button[name="update-avatar"]', avatar_crop_form);
                        update_btn.html(loading_button_html).prop("disabled", true);
                        $.ajax({
                            url: _base_url + 'api/customer/update_avatar',
                            type: 'POST',
                            data: $('#avatar-crop-popup-form').serialize(),
                            success: function(response) {
                                update_btn.html('Update').prop("disabled", false);
                                if (response.result && response.result.status === 'success') {
                                    var userPhoto = document.querySelector('.user-photo img');
                                    userPhoto.src = src; // not using link because of UX
                                    toast('Updated', response.result.message, 'success');
                                } else {
                                    toast('Failed', response.result.message, 'error');
                                }
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                update_btn.html('Update').prop("disabled", false);
                            }
                        });
                    }
                    closeCropper();
                });
            }
        });

        function closeCropper() {
            if (cropper) {
                cropper.destroy();
                cropper = null;
            }
            $('#avatar-crop-popup').hide(500);
        }
    </script>
@endpush
