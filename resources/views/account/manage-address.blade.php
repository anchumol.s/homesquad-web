@extends('layouts.main')
@section('title', 'Manage Address')
@section('content')
    <section>
        <div class="container">
            <div class="row inner-wrapper m-0">
                <div class="col-sm-12 top-steps-section">
                    <div class="d-flex page-title-section">
                        <div class="booking-page-title flex-grow-1">
                            <h3>Manage Address</h3>
                        </div>
                        <div class="step-back-icon"><a href="{{ url('profile') }}" class="back-arrow"
                                title="Click to Back">Back</a></div>
                        <div class="booking-steps">&nbsp;&nbsp; <a class="sp-btn show-add-address-popup"
                                data-action="new-address-popup">Add New</a></div>
                    </div>
                </div>
                <div class="col-sm-12 my-account-wrapper">
                    <form id="default_address_form" novalidate="novalidate">
                        <div class="row m-0 pt-3">
                            <div class="col-sm-8 n-personal-details m-auto" id="profile-address-list-holder">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('styles')
@endpush
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/profile.js?v=') . Config::get('version.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-locationpicker/0.1.12/locationpicker.jquery.min.js" integrity="sha512-KGE6gRUEc5VBc9weo5zMSOAvKAuSAfXN0I/djLFKgomlIUjDCz3b7Q+QDGDUhicHVLaGPX/zwHfDaVXS9Dt4YA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        $(document).ready(function() {
            fetchAddressList();
        });

        function showPosition(position) {
            $('#latitude').val(position.coords.latitude);
            $('#longitude').val(position.coords.longitude);
            locationPickr(position.coords.latitude, position.coords.longitude);
        }

        function showError(error) {
            switch (error.code) {
                case error.PERMISSION_DENIED:
                    $('.us3').locationpicker({
                        location: {
                            latitude: 25.055277,
                            longitude: 55.1586003
                        },
                        radius: 0,
                        inputBinding: {
                            latitudeInput: $('#latitude'),
                            longitudeInput: $('#longitude'),
                            radiusInput: $('.us3-radius'),
                            locationNameInput: $('.us3-address')
                        },
                        enableAutocomplete: true,
                        onchanged: function(currentLocation, radius, isMarkerDropped) {
                            // Uncomment line below to show alert on each Location Changed event
                            //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                        }
                    });
                    break;
                case error.POSITION_UNAVAILABLE:
                    console.log("Location information is unavailable.");
                    break;
                case error.TIMEOUT:
                    console.log("The request to get user location timed out.");
                    break;
                case error.UNKNOWN_ERROR:
                    console.log("An unknown error occurred.");
                    break;
            }
        }

        function locationPickr(latitude, longitude) {
            $('.us3').locationpicker({
                location: {
                    latitude: latitude,
                    longitude: longitude
                },

                radius: 0,
                inputBinding: {
                    latitudeInput: $('#latitude'),
                    longitudeInput: $('#longitude'),
                    radiusInput: $('.us3-radius'),
                    locationNameInput: $('.us3-address')
                },
                //markerIcon: _base_url +'images/picker.png',
                enableAutocomplete: true,
                onchanged: function(currentLocation, radius, isMarkerDropped) {
                    // Uncomment line below to show alert on each Location Changed event
                    //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                }
            });
        }
        $(document).ready(function() {
            if ($('#latitude').val() == '' || $('#longitude').val() == '') {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(showPosition, showError);
                } else {
                    x.innerHTML = "Geolocation is not supported by this browser.";
                }
            } else {
                locationPickr($('#latitude').val(), $('#longitude').val());
            }
        });
    </script>
@endpush
