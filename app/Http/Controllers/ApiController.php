<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class ApiController extends Controller
{
    public function customer_api_call(Request $request, $endpoint)
    {
        // to handle any ajax requests
        $data = [];
        /*************************************************************** */
        // customize data for real API
        foreach ($request->all() as $key => $value) {
            if ($key == "cleaning_materials") {
                $value = (boolean) $value; // convert int (1/0) to boolean
            }
            $data['params'][$key] = $value;
        }
        $response = customerApiCall($endpoint, $data, $request->method());
        /*************************************************************** */
        if (@$response['result']['status'] == 'success') {
            if ($endpoint == 'check_otp') {
                $session_data = [
                    'customer_id' => $response['result']['UserDetails']['id'],
                    'customer_name' => $response['result']['UserDetails']['UserName'],
                    'customer_token' => $response['result']['UserDetails']['token'],
                    'customer_avatar' => $response['result']['UserDetails']['image'],
                    'customer_mobile' => $response['result']['UserDetails']['mobile'],
                    'customer_email' => $response['result']['UserDetails']['email']
                ];
                session($session_data);
                if (isset($response['result']['UserDetails']['default_address_id'])) {
                    Session::put('customer_default_address_id', $response['result']['UserDetails']['token']);
                }
            } else if ($endpoint == 'update_customer_data') {
                Session::put('customer_token', $response['result']['UserDetails']['token']);
                Session::put('customer_name', $response['result']['UserDetails']['UserName']);
                Session::put('customer_email', $response['result']['UserDetails']['email']);
            } else if ($endpoint == 'name_entry') {
                Session::put('customer_token', $response['result']['UserDetails']['token']);
                Session::put('customer_name', $response['result']['UserDetails']['UserName']);
                Session::put('customer_email', $response['result']['UserDetails']['email']);
            } else if ($endpoint == 'update_avatar') {
                Session::put('customer_avatar', $response['result']['UserDetails']['image']);
            }
        }
        /*************************************************************** */
        if ($endpoint == 'customer_logout') {
            Session::flush();
        }
        /*************************************************************** */
        return $response;
    }
    public function hijack_customer_login(Request $request)
    {
        $params['params']['id'] = $request->id;
        $params['params']['token'] = $request->token;
        $response = customerApiCall('validate_token', $params);
        if (@$response['result']['status'] == "success") {
            $session_data = [
                'customer_id' => $response['result']['customer']['customer_id'],
                'customer_name' => $response['result']['customer']['customer_name'],
                'customer_token' => $response['result']['customer']['oauth_token'],
                'customer_avatar' => $response['result']['customer']['customer_photo_file'],
                'customer_mobile' => $response['result']['customer']['mobile_number_1'],
                'customer_email' => $response['result']['customer']['email_address']
            ];
            session($session_data);
            return 'Login session saved successfully !';
        } else {
            return 'Expired Token or Invalid Customer !';
        }
    }
}
